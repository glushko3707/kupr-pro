<?php

namespace App\Jobs;

use App\Models\ShortLink;
use App\Models\Visit;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class VisitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $code;
    /**
     * Create a new job instance.
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $shortLink = ShortLink::query()->find($this->code);
        $fullLink = $shortLink->link;

        $date = [];

        parse_str(parse_url($fullLink, PHP_URL_QUERY), $parts);

        $date['code'] = $this->code;
        $date['utm_source'] = $parts['utm_source'] ?? null;
        $date['utm_medium'] = $parts['utm_medium'] ?? null;
        $date['phone'] = $parts['phone'] ?? null;
        $date['link'] = $fullLink;

        Visit::query()->updateOrCreate([
            'code' =>  $date['code'],
            'created_at' =>  now(),
        ],$date);
    }
}
