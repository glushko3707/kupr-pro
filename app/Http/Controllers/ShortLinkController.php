<?php

namespace App\Http\Controllers;

use App\Jobs\VisitJob;
use App\Models\ShortLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use str_increment\StrIncrement;

class ShortLinkController extends Controller
{
    public function getLink(Request $request){

        // Используем блокировку, чтобы избежать конкурентного доступа к таблице
        DB::beginTransaction();
        try {
            // Получаем последнюю запись из таблицы
            $lastSetting = ShortLink::query()->orderBy('code', 'desc')->latest()->first();
            $code = StrIncrement::NewStr($lastSetting->code, 'lowercase');
            // Создаем или обновляем запись с новым кодом

            ShortLink::query()->updateOrCreate([
                'link' => $request->linkFull,
            ],[
                'code' => $code,
            ]);
            // Фиксируем изменения в базе данных
            DB::commit();
        } catch (\Exception $e) {
            // Откатываем изменения в случае ошибки
            DB::rollBack();
            // Выводим сообщение об ошибке
            echo $e->getMessage();
        }

        $code = $code ?? 'error';

        return response($code, 200)
            ->header('Content-Type', 'text/plain');
    }

    public function redirect($code){
        VisitJob::dispatch($code);

// Создаем массив с именами
        $names = [
            "Авада",
            "Базилиск",
            "Волдеморт",
            "Гриффиндор",
            "Дамблдор",
            "Хедвига",
            "Альбус",
            "Забини",
            "Игнатия",
            "Квирелл",
            "Луна",
            "Беллатриса",
            "Невилл",
            "Оливандер",
            "Поттер",
            "Рон",
            "Слизерин",
            "Трелони",
            "Умбридж",
            "Филч",
            "Хагрид",
            "Цветочек",
            "Чарли",
            "Люциус",
            "Эррол",
            "Якс",
            "Минерва",
            "Беллатрикс",
            "Виктор",
            "Гермиона",
            "Драко",
            "Еван",
            "Жан",
            "Зевс",
            "Ирма",
            "Кроули",
            "Лилич",
            "Малфой",
            "Нимфадор",
            "Октавиан",
            "Перси",
            "Ремус",
            "Сириус",
            "Тонкс",
            "Уилл",
            "Фред",
            "Хелга",
            "Целестина",
            "Чжо",
            "Шмидт",
            "Эдгар",
            "Янур"
        ];

        $linker = ShortLink::query()->find($code,['link'])->link . '&username=' . $names[rand(0,49)];

        return redirect($linker);
    }
}
